# 多功能下拉选择插件


## 功能特点


* 基于jQuery、Bootstrap2、3开发

* 输入自动查找

* 列表结果分页展示

* 使用键盘快速操作基本功能及分页功能

* 多项选择以标签（Tag）形式展现

* 结果列表自动判断屏幕边缘，避免内容超出可视范围

* 浏览器兼容：IE8+、Chrome、Firefox等

插件基于jQuery开发，建议在Bootstrap2、3环境下使用

更多实例、文档请访问：https://terryz.github.io

![SelectPage](https://terryz.github.io/image/SelectPage.png "在这里输入图片标题")

